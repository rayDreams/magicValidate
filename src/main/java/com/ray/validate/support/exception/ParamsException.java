package com.ray.validate.support.exception;

/**
 * @author bo shen
 * @Description: 参数校验错误编码
 * @Class: ParamsException
 * @Package tf56.antcolonyums.validate.support.exception
 * @date 2019/11/19 15:04
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class ParamsException extends RuntimeException {

    /**
     * 统一默认错误码
     **/
    public static final Integer error = 500;


    /**
     * 错误编码
     **/
    private int code;

    /**
     * 构造一个业务异常类
     *
     * @param message 错误信息
     */
    public ParamsException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    /**
     * 构造一个业务异常类
     *
     * @param message 错误信息
     */
    public ParamsException(String message) {
        super(message);
        this.code = error;
    }

    /**
     * 返回错误码
     *
     * @return 错误码
     */
    public Integer getCode() {
        return code;
    }
}
