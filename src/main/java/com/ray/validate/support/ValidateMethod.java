package com.ray.validate.support;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateMethod {
    /***
     * 参数允许为空校验
     * @return
     */
    boolean paramsNullCheck() default false;
}
