package com.ray.validate.support;

import com.ray.validate.support.aspect.ValidateAspect;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({ValidateAspect.class})
public @interface EnableValidate {

}
