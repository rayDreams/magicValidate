package com.ray.validate.support.rule;


import com.ray.validate.ConditionNull;
import com.ray.validate.support.utils.PropertyUtils;
import com.ray.validate.support.utils.ValidateUtil;

import java.util.Arrays;

/**
 * @author bo shen
 * @Description: 条件空
 * @Class: ConditionNullRule
 * @Package tf56.antcolonyums.validate.support.rule
 * @date 2019/11/19 16:01
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class ConditionNullRule implements Rule<ConditionNull> {

    @Override
    public void rule(Object bean, String fieleName, ConditionNull conditionNull) {
        if (conditionNull.contitions().length > 0) {
            String objectValue = PropertyUtils.getProperty(bean, conditionNull.fieldName());
            if (!Arrays.asList(conditionNull.contitions()).contains(objectValue)) {
                ValidateUtil.notNull(PropertyUtils.getProperty(bean, fieleName), conditionNull.message());
            }
        }
    }

    @Override
    public void rule(Object value, ConditionNull conditionNull) {
        
    }
}
