package com.ray.validate.support.rule;

import java.lang.annotation.Annotation;

/**
 * @author bo shen
 * @Description: 校验规则
 * @Class: Rule
 * @Package tf56.antcolonyums.validate.support.rule
 * @date 2019/11/19 15:32
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public interface Rule<E extends  Annotation> {
    /**
     *
     * @param bean 值
     * @param annotation 附加条件
     */
    void rule(Object bean, String fieleName, E annotation);

    /**
     *
     * @param value 值
     * @param annotation 附加条件
     */
    void rule(Object value, E annotation);

}
