package com.ray.validate.support.rule;

import com.ray.validate.ConditionNotNull;
import com.ray.validate.support.utils.PropertyUtils;
import com.ray.validate.support.utils.ValidateUtil;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import java.util.Arrays;

/**
 * @author bo shen
 * @Description: 条件不为空
 * @Class: ConditionNotNullRule
 * @Package tf56.antcolonyums.validate.support.rule
 * @date 2019/11/19 16:01
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class ConditionNotNullRule implements Rule<ConditionNotNull> {

    @Override
    public void rule(Object bean, String fieleName, ConditionNotNull conditionNotNull) {
        if (!ObjectUtils.isEmpty(conditionNotNull) && StringUtils.hasLength(conditionNotNull.fieldName())) {
            if (conditionNotNull.contitions().length > 0) {
                String objectValue = PropertyUtils.getProperty(bean, conditionNotNull.fieldName());
                if (Arrays.asList(conditionNotNull.contitions()).contains(objectValue)) {
                    ValidateUtil.notNull(PropertyUtils.getProperty(bean, fieleName), conditionNotNull.message());
                }
            }
        }
    }

    @Override
    public void rule(Object value, ConditionNotNull conditionNotNull) {

    }
}
