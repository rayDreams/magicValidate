package com.ray.validate.support.rule;


import com.ray.validate.support.utils.PropertyUtils;
import com.ray.validate.support.utils.ValidateUtil;
import org.hibernate.validator.constraints.Length;

/**
 * @author bo shen
 * @Description: Length校验
 * @Class: FutureRule
 * @Package com.ray.validate.support.rule
 * @date 2020/3/18 9:52
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class LengthRule implements Rule<Length> {

    @Override
    public void rule(Object bean, String fieleName, Length annotation) {
        String value = PropertyUtils.getProperty(bean, fieleName);
        if (value != null) {
            int length = value.length();
            ValidateUtil.isTrue(!(length >= annotation.min() && length <= annotation.max()), annotation.message());
        }
    }

    @Override
    public void rule(Object value, Length annotation) {
        String name = String.valueOf(value);
        if (name != null) {
            int length = name.length();
            ValidateUtil.isTrue(!(length >= annotation.min() && length <= annotation.max()), annotation.message());
        }
    }
}
