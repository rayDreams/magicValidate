package com.ray.validate.support.rule;

import com.ray.validate.ConditionNotBlank;
import com.ray.validate.support.utils.PropertyUtils;
import com.ray.validate.support.utils.ValidateUtil;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import java.util.Arrays;

/**
 * @author bo shen
 * @Description: 条件不能为空
 * @Class: ConditionNotBlankRule
 * @Package tf56.antcolonyums.validate.support.rule
 * @date 2019/11/19 15:49
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class ConditionNotBlankRule implements Rule<ConditionNotBlank>{
    @Override
    public void rule(Object bean, String fieleName, ConditionNotBlank conditionNotBlank) {
        if(!ObjectUtils.isEmpty(conditionNotBlank) && StringUtils.hasLength(conditionNotBlank.fieldName())){
            if(conditionNotBlank.contitions().length >0){
                String objectValue = PropertyUtils.getProperty(bean,conditionNotBlank.fieldName());
                if(Arrays.asList(conditionNotBlank.contitions()).contains(objectValue)){
                    ValidateUtil.hasLength(PropertyUtils.getProperty(bean,fieleName),conditionNotBlank.message());
                }
            }
        }
    }

    @Override
    public void rule(Object value, ConditionNotBlank conditionNotBlank) {
    }
}
