package com.ray.validate.support.rule;


import com.ray.validate.support.utils.PropertyUtils;
import com.ray.validate.support.utils.ValidateUtil;
import org.hibernate.validator.constraints.Range;

/**
 * @author bo shen
 * @Description: Range校验
 * @Class: FutureRule
 * @Package com.ray.validate.support.rule
 * @date 2020/3/18 9:52
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class RangeRule implements Rule<Range> {

    @Override
    public void rule(Object bean, String fieleName, Range annotation) {
        ValidateUtil.max(PropertyUtils.getProperty(bean,fieleName),annotation.max(),annotation.message());
        ValidateUtil.min(PropertyUtils.getProperty(bean,fieleName),annotation.min(),annotation.message());
    }

    @Override
    public void rule(Object value, Range annotation) {
        ValidateUtil.max(String.valueOf(value),annotation.max(),annotation.message());
        ValidateUtil.min(String.valueOf(value),annotation.min(),annotation.message());
    }
}
