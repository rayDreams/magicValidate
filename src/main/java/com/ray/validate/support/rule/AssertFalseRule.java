package com.ray.validate.support.rule;

import com.ray.validate.support.utils.PropertyUtils;
import com.ray.validate.support.utils.ValidateUtil;

import javax.validation.constraints.AssertFalse;

/**
 * @author bo shen
 * @Description: AssertFalse 校验
 * @Class: DecimalMinRule
 * @Package com.ray.validate.support.rule
 * @date 2020/3/18 8:41
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class AssertFalseRule implements Rule<AssertFalse> {
    @Override
    public void rule(Object bean, String fieleName, AssertFalse annotation) {
        ValidateUtil.isTrue(Boolean.valueOf(PropertyUtils.getProperty(bean,fieleName)),annotation.message());
    }

    @Override
    public void rule(Object value, AssertFalse annotation) {
        ValidateUtil.isTrue((Boolean) value,annotation.message());
    }
}
