package com.ray.validate.support.rule;



import com.ray.validate.support.utils.PropertyUtils;
import com.ray.validate.support.utils.ValidateUtil;

import javax.validation.constraints.Size;

/**
 * @author bo shen
 * @Description: 对象不为空 校验
 * @Class: NotBlankRule
 * @Package tf56.antcolonyums.validate.support.rule
 * @date 2019/11/19 15:36
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class SizeRule implements Rule<Size>{
    @Override
    public void rule(Object bean, String fieleName,Size annotation) {
        ValidateUtil.size(PropertyUtils.getProperty(bean,fieleName),annotation.min(),annotation.max(),annotation.message());
    }
}
