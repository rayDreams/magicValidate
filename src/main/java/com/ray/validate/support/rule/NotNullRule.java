package com.ray.validate.support.rule;



import com.ray.validate.support.utils.PropertyUtils;
import com.ray.validate.support.utils.ValidateUtil;

import javax.validation.constraints.NotNull;

/**
 * @author bo shen
 * @Description: 对象不为空 校验
 * @Class: NotBlankRule
 * @Package tf56.antcolonyums.validate.support.rule
 * @date 2019/11/19 15:36
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class NotNullRule implements Rule<NotNull>{
    @Override
    public void rule(Object bean, String fieleName,NotNull annotation) {
        ValidateUtil.notNull(PropertyUtils.getProperty(bean,fieleName),annotation.message());
    }

    @Override
    public void rule(Object value, NotNull annotation) {
        ValidateUtil.notNull(value,annotation.message());
    }
}
