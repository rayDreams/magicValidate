package com.ray.validate.support.rule;

import com.ray.validate.support.utils.PropertyUtils;
import com.ray.validate.support.utils.ValidateUtil;

import javax.validation.constraints.DecimalMin;

/**
 * @author bo shen
 * @Description: DecimalMin 校验
 * @Class: DecimalMinRule
 * @Package com.ray.validate.support.rule
 * @date 2020/3/18 8:41
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class DecimalMaxRule implements Rule<DecimalMin> {
    @Override
    public void rule(Object bean, String fieleName, DecimalMin annotation) {
        ValidateUtil.max(PropertyUtils.getProperty(bean,fieleName),Double.valueOf(annotation.value()),annotation.message());
    }

    @Override
    public void rule(Object value, DecimalMin annotation) {
        ValidateUtil.max(String.valueOf(value),Double.valueOf(annotation.value()),annotation.message());
    }
}
