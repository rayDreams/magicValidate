package com.ray.validate.support.rule;


import org.hibernate.validator.constraints.Email;

/**
 * @author bo shen
 * @Description: Email校验
 * @Class: FutureRule
 * @Package com.ray.validate.support.rule
 * @date 2020/3/18 9:52
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class EmailRule implements Rule<Email> {

    @Override
    public void rule(Object bean, String fieleName, Email annotation) {

    }

    @Override
    public void rule(Object value, Email annotation) {

    }
}
