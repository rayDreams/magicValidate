package com.ray.validate.support.rule;

import com.ray.validate.ConditionIn;
import com.ray.validate.support.exception.ParamsException;
import com.ray.validate.support.utils.PropertyUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.Arrays;

/**
 * @author bo shen
 * @Description: 条件在列表中
 * @Class: ConditionInRule
 * @Package tf56.antcolonyums.validate.support.rule
 * @date 2019/11/19 16:00
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class ConditionInRule implements Rule<ConditionIn> {

    @Override
    public void rule(Object bean, String fieleName, ConditionIn conditionIn) {
        if (!ObjectUtils.isEmpty(conditionIn)) {
            String objectValue = PropertyUtils.getProperty(bean, fieleName);
            if (StringUtils.hasLength(objectValue) && !Arrays.asList(conditionIn.values()).contains(objectValue)) {
                throw new ParamsException(conditionIn.message());
            }
        }
    }

    @Override
    public void rule(Object value, ConditionIn conditionIn) {

    }
}
