package com.ray.validate.support.rule;


import com.ray.validate.support.utils.PropertyUtils;
import com.ray.validate.support.utils.ValidateUtil;

import javax.validation.constraints.Max;

/**
 * @author bo shen
 * @Description: 最大值校验 校验
 * @Class: NotBlankRule
 * @Package tf56.antcolonyums.validate.support.rule
 * @date 2019/11/19 15:36
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class MaxRule implements Rule<Max>{
    @Override
    public void rule(Object bean, String fieleName,Max annotation) {
        ValidateUtil.max(PropertyUtils.getProperty(bean,fieleName),annotation.value(),annotation.message());
    }

    @Override
    public void rule(Object value, Max annotation) {
        ValidateUtil.max(String.valueOf(value),annotation.value(),annotation.message());
    }
}
