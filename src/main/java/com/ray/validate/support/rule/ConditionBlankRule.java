package com.ray.validate.support.rule;

import com.ray.validate.ConditionBlank;
import com.ray.validate.support.utils.PropertyUtils;
import com.ray.validate.support.utils.ValidateUtil;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.Arrays;

/**
 * @author bo shen
 * @Description: 条件不能为空
 * @Class: ConditionBlankRule
 * @Package tf56.antcolonyums.validate.support.rule
 * @date 2019/11/19 16:00
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class ConditionBlankRule implements  Rule<ConditionBlank>{

    @Override
    public void rule(Object bean, String fieleName, ConditionBlank conditionBlank) {
        if(!ObjectUtils.isEmpty(conditionBlank) && StringUtils.hasLength(conditionBlank.fieldName())){
            if(conditionBlank.contitions().length >0){
                String objectValue = PropertyUtils.getProperty(bean,conditionBlank.fieldName());
                if(!Arrays.asList(conditionBlank.contitions()).contains(objectValue)){
                    ValidateUtil.hasLength((String)PropertyUtils.getProperty(bean,fieleName),conditionBlank.message());
                }
            }
        }
    }

    @Override
    public void rule(Object value, ConditionBlank annotation) {
    }
}
