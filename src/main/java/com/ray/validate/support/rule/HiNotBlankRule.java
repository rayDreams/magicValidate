package com.ray.validate.support.rule;

import com.ray.validate.support.utils.PropertyUtils;
import com.ray.validate.support.utils.ValidateUtil;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.util.ObjectUtils;



/**
 * @author bo shen
 * @Description: 字符串不为空 校验
 * @Class: NotBlankRule
 * @Package tf56.antcolonyums.validate.support.rule
 * @date 2019/11/19 15:36
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
public class HiNotBlankRule implements Rule<NotBlank>{

    @Override
    public void rule(Object bean,String fieleName, NotBlank annotation) {
        if(!ObjectUtils.isEmpty(annotation)){
            ValidateUtil.hasLength(PropertyUtils.getProperty(bean,fieleName),annotation.message());
        }
    }

    @Override
    public void rule(Object value, NotBlank annotation) {
        ValidateUtil.hasLength(String.valueOf(value),annotation.message());
    }
}
