package com.ray.validate;

import java.lang.annotation.*;

/**
 * @author bo shen
 * @Description: 值再列表中
 * @Class: ConditionIn
 * @Package tf56.odsFacade.validate
 * @date 2019/7/18 20:03
 * @company <p>杭州传化陆鲸科技有限公司</p>
 * @updateRecord time(修改时间)  author(修改人)   desc(修改内容)
 */
@Documented
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ConditionIn {
    String message() default "";
    String[] values() default {};
}
