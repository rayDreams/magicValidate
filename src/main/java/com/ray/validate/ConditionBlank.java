package com.ray.validate;

import java.lang.annotation.*;

/**
 * 条件
 */
@Documented
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ConditionBlank {
    String message() default "";

    String fieldName() default "";

    String[] contitions() default {};
}
